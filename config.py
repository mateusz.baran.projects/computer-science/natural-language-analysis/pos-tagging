from pathlib import Path

PROJECT_DIR = Path(__file__).parent.absolute()
DATA_DIR = PROJECT_DIR / 'data'
OUTPUT_DIR = PROJECT_DIR / 'output'

RAW_TRAIN_DATA_DIR = DATA_DIR / 'raw' / 'wiki_train_34_categories_data'
RAW_TEST_DATA_DIR = DATA_DIR / 'raw' / 'wiki_test_34_categories_data'

PREPROCESSED_DATA_PATH = DATA_DIR / 'preprocessed' / 'dataset.csv'
PROCESSED_DATA_PATH = DATA_DIR / 'processed' / 'dataset.pkl'

POLISH_STOP_WORDS_PATH = DATA_DIR / 'stopwords' / 'polish.txt'

POLEVAL_TEST_ANALYZED_DATA_PATH = DATA_DIR / 'poleval' / 'test-analyzed.xml'
POLEVAL_TEST_RAW_DATA_PATH = DATA_DIR / 'poleval' / 'test-raw.txt'

MORPHODITA_BIN_PATH = PROJECT_DIR / 'morphodita' / 'MorphoDiTaPL'
MORPHODITA_TAGGER_PATH = PROJECT_DIR / 'morphodita' / 'pl.tagger'
