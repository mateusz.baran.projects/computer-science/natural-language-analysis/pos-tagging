### FOLD  1: tag.xml (tag) v. ref.xml (ref)
10000 iterations...
20000 iterations...
PolEval 2017 competition scores
-------------------------------
POS accuracy (Subtask A score):         92.5298%
POS accuracy (known words):     93.3672%
POS accuracy (unknown words):   52.2282%
Lemmatization accuracy (Subtask B score):       96.9162%
Lemmatization accuracy (known words):   97.5891%
Lemmatization accuracy (unknown words):         64.5276%
Overall accuracy (Subtask C score):     94.7230%
----
REF-toks        27563
KN      97.9647%
KN_POS_SC_LOWER 97.4483%
KN_SC_LOWER     93.3672%
KN_SEG_CHANGE   0.7407%
KN_SL_LOWER     97.5891%
KN_WC_LOWER     93.4005%
POS_SC_LOWER    96.9669%
POS_WC_LOWER    96.9669%
SC_LOWER        92.5298%
SEG_CHANGE      1.1102%
SEG_NOCHANGE    98.8898%
SL_CASE_CAT_HEUR  97.2064%
SL_LOWER        96.9162%
SL_NOCASE_CAT_HEUR     97.8957%
SL_NOCASE_LOWER 97.2644%
UNK     2.0353%
UNK_POS_SC_LOWER  73.7968%
UNK_SC_LOWER    52.2282%
UNK_SEG_CHANGE  18.8948%
UNK_SL_LOWER    64.5276%
UNK_WC_LOWER    52.2282%
WC_LOWER        92.5625%
WL_LOWER        96.9162%
WC_UPPER        93.6727%
AVG weak lemma lower bound 96.9162%
AVG KN strong lemma lower bound 97.5891%
AVG UNK strong lemma lower bound        64.5276%
AVG strong lemma lower bound    96.9162%
AVG strong lemma nocase lower bound     97.2644%
AVG strong lemma case concat heur       97.2064%
AVG strong lemma nocase concat heur     97.8957%
AVG weak corr lower bound  92.5625%
AVG weak corr upper bound  93.6727%
AVG UNK weak corr lower bound   52.2282%
AVG UNK weak corr upper bound   71.1230%
AVG KN  weak corr lower bound   93.4005%
AVG KN  weak corr upper bound   94.1412%
AVG POS strong corr lower bound 96.9669%
AVG percentage UNK     2.0353%
AVG percentage seg change  1.1102%
