FROM clarinpl/python:3.6

WORKDIR /app
ENV PYTHONPATH=.:$PYTHONPATH

RUN apt-get update && apt-get install -y \
    morfeusz2 \
    python3-morfeusz2

COPY ./ ./

RUN pip install -r requirements.txt

CMD ["python", "morphodita/MorphoDiTaPL", "morphodita/pl.tagger", "morphodita/file_in.txt", "morphodita/file_out.txt"]
