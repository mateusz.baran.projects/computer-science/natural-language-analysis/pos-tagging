import re


class Word:
    def __init__(self, word: str):
        self.word = word

    def __str__(self):
        return self.word

    __repr__ = __str__


class Sentence:
    def __init__(self, sent: str):
        self.sent = sent

    def __str__(self):
        return self.sent

    __repr__ = __str__

    def __iter__(self):
        self.sent = re.sub(r'([.,:;<>/\\\[\]{}?!()_+=*`~"\'])', r' \1 ', self.sent)
        # self.sent = re.sub(r'-', r'', self.sent)  # TODO
        self.sent = re.sub(r' +', r' ', self.sent)
        self.sent = self.sent.strip()

        for word in self.sent.split(' '):
            yield Word(word)


class Document:
    KNOWN_ACRONYMS = ['chorw', 'wł', 'km', 'tys', 'np', 'tzw', 'ur', 'zm', 'USA', 'vol']

    def __init__(self, doc: str):
        self.doc = doc

    def __str__(self):
        return self.doc

    __repr__ = __str__

    def __iter__(self):
        known_acronyms = "".join(f"(?<!{x})" for x in self.KNOWN_ACRONYMS)

        self.doc = re.sub(r'\n(?!\n)+', r' ', self.doc)
        self.doc = re.sub(r'\t+', r'\n', self.doc)
        self.doc = re.sub(rf'{known_acronyms}([.])', r'\1\n', self.doc)
        self.doc = re.sub(r'([!?])', r'\1\n', self.doc)
        self.doc = re.sub(r'\n( *\n*)*', r'\n', self.doc)
        self.doc = self.doc.strip()

        for sent in self.doc.splitlines():
            yield Sentence(sent)


if __name__ == '__main__':
    import config

    with config.RAW_TRAIN_DATA_DIR.joinpath('Amerykanscy-prozaicy_111551.txt').open() as file:
        document = file.read()

    for sentence in Document(document):
        print(str(sentence))
        print(list(sentence))
        print('-----------------------------------------------------')
