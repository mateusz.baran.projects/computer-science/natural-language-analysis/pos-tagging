import subprocess
from tempfile import NamedTemporaryFile
from typing import Dict, List
from xml.etree.ElementTree import ElementTree, fromstring

import config


def to_xml_tags(text: str) -> ElementTree:
    with NamedTemporaryFile('r+') as file_in:
        file_in.write(text)
        file_in.seek(0)

        with NamedTemporaryFile('w+') as file_out:
            subprocess.call([config.MORPHODITA_BIN_PATH,
                             config.MORPHODITA_TAGGER_PATH,
                             file_in.name,
                             file_out.name])
            text_tags = file_out.read()

    return ElementTree(fromstring(text_tags))


def to_tags(text: str) -> List[Dict[str, str]]:
    tags = to_xml_tags(text)

    tok_lst = [{'orth': tok.find('orth').text,
                'base': tok.find('lex').find('base').text,
                'tag': tok.find('lex').find('ctag').text}
               for tok in tags.iter('tok')]

    return tok_lst


if __name__ == '__main__':
    print('Print', to_tags('Kot Filemon robi piękny \n\n stolik'))
