import subprocess

from config import OUTPUT_DIR, POLEVAL_TEST_RAW_DATA_PATH

# Fist - run docker with krnnt tagger
# docker run -p 9003:9003 djstrong/krnnt

with open(POLEVAL_TEST_RAW_DATA_PATH, 'r') as file:
    text = file.read()

command = f'curl -XPOST localhost:9003/?output_format=xces -d @{POLEVAL_TEST_RAW_DATA_PATH}'
cmd = command.split()

process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
output, error = process.communicate()
with open(OUTPUT_DIR / 'krnnt.xml', 'wb') as output_file:
    output_file.write(output)
